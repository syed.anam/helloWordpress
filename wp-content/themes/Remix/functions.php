<?php

require_once('/codestar/cs-framework.php');
require_once('/inc/custom_post.php');

// for post thumbnail
	add_theme_support('post-thumbnails');

	function load_css_js(){
		//<!-- Font Awesome file include -->
		wp_enqueue_style('awesome',get_template_directory_uri().'/assets/css/font-awesome.min.css',array(),'1.0.0','all');
		wp_enqueue_style('flaticon',get_template_directory_uri().'/assets/css/flaticon.css',array(),'1.0.0','all');
		
		//<!-- For Boxer -->
		wp_enqueue_style('boxer',get_template_directory_uri().'/assets/css/jquery.fs.boxer.min.css',array(),'1.0.0','all');
		
		//<!-- Bootstrap css file include -->
		wp_enqueue_style('bootstrap',get_template_directory_uri().'/assets/css/bootstrap.min.css',array(),'1.0.0','all');
		
		//<!-- Animate css file include -->
		wp_enqueue_style('animate',get_template_directory_uri().'/assets/css/animate.min.css',array(),'1.0.0','all');
		
		//<!-- Main style css file include -->
		wp_enqueue_style('style',get_template_directory_uri().'/assets/css/style.css',array(),'1.0.0','all');
		
		//<!-- Responsive css file include -->
		wp_enqueue_style('responsive',get_template_directory_uri().'/assets/css/responsive.css',array(),'1.0.0','all');
		
		
		//<!-- For Js Library -->
		wp_enqueue_script('jquery');
		
		//<!-- For Modernizr -->
		wp_enqueue_script('modernizr',get_template_directory_uri().'/assets/js/modernizr.js',array(),'1.0.0',true);
		
		//<!-- For Js Plugins -->
		wp_enqueue_script('plugins',get_template_directory_uri().'/assets/js/plugins.js',array(),'1.0.0',true);
		
		//<!-- Include OnePagenNav.js -->
		wp_enqueue_script('OnePagenNav',get_template_directory_uri().'/assets/js/OnePagenNav.js',array(),'1.0.0',true);
		
		//<!-- For Apper Js -->
		wp_enqueue_script('wow',get_template_directory_uri().'/assets/js/wow.min.js',array(),'1.0.0',true);
		
		//<!-- Include Functions.js -->
		wp_enqueue_script('functions',get_template_directory_uri().'/assets/js/functions.js',array(),'1.0.0',true);
		
		//<!-- Jquery Piechart -->
		wp_enqueue_script('easypiechart',get_template_directory_uri().'/assets/js/jquery.easypiechart.min.js',array(),'1.0.0',true);
		
		//<!-- Google Maps Script  -->
		//wp_enqueue_script('gmap3',get_template_directory_uri().'/assets/js/gmap3.min.js',array(),'1.0.0',true);
		//wp_enqueue_script('http://maps.google.com/maps/api/js?sensor=true"');

		
	}
	add_action('wp_enqueue_scripts','load_css_js');
	
	function extra_js(){
		?>
		
		<script type="text/javascript">

		    jQuery('#main-nav').onePageNav({
                 currentClass: 'active',
                 changeHash: false,
                 scrollSpeed: 1200,
                 scrollOffset: 80,
				 filter: ':not(.sub-menu a, .not-home)'
            });

			jQuery('.carousel').carousel({
		        interval: 8000
		    })	

			/*  - wow animation wow.js 
		    ---------------------------------------------------*/
		    var wow = new WOW (
		    {
		          boxClass:     'wow',      // animated element css class (default is wow)
		          animateClass: 'animated', // animation css class (default is animated)
		          offset:       0,          // distance to the element when triggering the animation (default is 0)
		          mobile:       false       // trigger animations on mobile devices (true is default)
		      }
		    );
		    wow.init();

		    /*  - wow animation wow.js End 
		    ---------------------------------------------------*/

		</script>
		
		<?php		
	}
	add_action('wp_footer','extra_js',100);
