<?php
//For dynamic logo upload
add_theme_support( 'custom-header' );
//For dynamic logo upload


//for menu

register_nav_menus(array(
	//'key'=>'value';
	'header_menu'=>'This is our header Menu',
	'footer_menu'=>'This is our footer Menu',

));
//for menu

function theme_js_css(){
	wp_enqueue_style('style',get_template_directory_uri().'/style.css',array(),'v.1.0.2.0','all');
	wp_enqueue_style('bootstrap',get_template_directory_uri().'/css/bootstrap.css',array(),'v.1.0.2.0','all');
	wp_enqueue_style('bootstrap.min',get_template_directory_uri().'/css/bootstrap.min.css',array(),'v.1.0.2.0','all');
	wp_enqueue_style('jquery-ui',get_template_directory_uri().'/css/jquery-ui.css',array(),'v.1.0.2.0','all');
	wp_enqueue_style('jquery-ui.min',get_template_directory_uri().'/css/jquery-ui.min.css',array(),'v.1.0.2.0','all');
	
	wp_enqueue_script('jquery');
	wp_enqueue_script('jquery-ui',get_template_directory_uri().'/js/jquery-ui.js',array(),'v.1.0.2.0',true);
	wp_enqueue_script('template_decoretion',get_template_directory_uri().'/js/template_decoretion.js',array(),'v.1.0.2.0',true);
	wp_enqueue_script('bootstrap',get_template_directory_uri().'/js/bootstrap.js',array(),'v.1.0.2.0',true);
	wp_enqueue_script('bootstrap.min',get_template_directory_uri().'/js/bootstrap.min.js',array(),'v.1.0.2.0',true);
	
	
}
add_action('wp_enqueue_scripts','theme_js_css');
