<?php

function theme_js_css(){
	
	wp_enqueue_style('template',get_template_directory_uri().'/css/template.css',array(),'v3.3','all');
	wp_enqueue_style('bootstrap',get_template_directory_uri().'/css/bootstrap.css',array(),'v3.3','all');
	wp_enqueue_style('bootstrap-min',get_template_directory_uri().'/css/bootstrap.min.css',array(),'v3.3','all');
	wp_enqueue_style('jquery-ui',get_template_directory_uri().'/css/jquery-ui.css',array(),'v3.3','all');
	wp_enqueue_style('jquery-ui-min',get_template_directory_uri().'/css/jquery-ui.min.css',array(),'v3.3','all');
	
	
	//wp_enqueue_script('jquery',get_template_directory_uri().'/js/jquery.js',array(),'v3.3',false);
	wp_enqueue_script('jquery');
	wp_enqueue_script('jquery-ui',get_template_directory_uri().'/js/jquery-ui.js',array(),'v3.3',true);
	wp_enqueue_script('template_decoretion',get_template_directory_uri().'/js/template_decoretion.js',array(),'v3.3',true);
	wp_enqueue_script('bootstrap',get_template_directory_uri().'/js/bootstrap.js',array(),'v3.3',true);
	wp_enqueue_script('bootstrap-min',get_template_directory_uri().'/js/bootstrap.min.js',array(),'v3.3',true);
	
}
	add_action('wp_enqueue_scripts','theme_js_css');
	

	add_theme_support( 'custom-header' );
	
	register_nav_menus(array(
	
		'Header_menu'=>'This is header menu',
		'footer_menu'=>'This is footer menu',
		'Side_menu'=>'This is side menu',
	));
?>