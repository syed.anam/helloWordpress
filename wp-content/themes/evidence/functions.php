<?php

function css_file_js(){
	wp_enqueue_style('styletmplate',get_template_directory_uri().'/css/main.css',array(),'2.0.1.3','all');
	
	wp_enqueue_script('jquery');
	wp_enqueue_script('jqueryjs',get_template_directory_uri().'/js/jquery.min.js',array(),'2.0.3',true);
	wp_enqueue_script('mainjs',get_template_directory_uri().'/js/main.js',array(),'2.0.3',true);
	
	
}
add_action('wp_enqueue_scripts','css_file_js');


register_nav_menus(array(
	
		'Header_menu'=>'This is header menu',		
		'Side_menu'=>'This is side menu',
	));



