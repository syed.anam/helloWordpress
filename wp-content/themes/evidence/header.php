<!DOCTYPE HTML>
<html>
	<head>
		<title>IDB Bisew</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		
		<?php wp_head()?>
	</head>
	<body>
	
	<!-- Header -->
			<header id="header">
				<a href="index.html" class="title">Sabbir Ahmed</a>
				<nav>				
					<?php
					
					wp_nav_menu(array(
					
						'theme_location'=>'Header_menu',
						
						'menu_id'=>'header',
					));
					?>
					
				</nav>
			</header>