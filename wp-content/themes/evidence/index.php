<?php get_header();?>

		<!-- Sidebar -->
			<section id="sidebar">		
			<?php
					
					wp_nav_menu(array(
					
						'theme_location'=>'Side_menu',						
						'menu_class'=>'sidebar inner',
						'menu_id'=>'sidebar inner',
					));
					?>
				<div class="inner">
					<nav>
						<ul>
							<li><a href="#intro">Welcome</a></li>
							<li><a href="#one">Who we are</a></li>
							<li><a href="#three">Get in touch</a></li>
						</ul>
					</nav>
				</div>
			</section>

		<!-- Wrapper -->
			<div id="wrapper">

				<!-- Intro -->
					<section id="intro" class="wrapper style1 fullscreen fade-up">
						<div class="inner">
							<h1>Sabbir Ahmed</h1>
							<p>IDB Bisew Round-27 Student
							</p>
							<ul class="actions">
								<li><a href="#one" class="button scrolly">Learn more</a></li>
							</ul>
						</div>
					</section>

				<!-- One -->
					<section id="one" class="wrapper style2 spotlights">
					
					<?php
							
							query_posts(array(
							
							'post_type'=>'post',
							));
							if(have_posts()) : while(have_posts()) : the_post();
							?>
						<section>	
							
							<div class="content">
								<div class="inner">
									<h2><?php the_title(); ?> </h2>
									<p><?php the_content(); ?></p>
									<ul class="actions">
										<li><a href="#" class="button">Learn more</a></li>
									</ul>
								</div>
							</div>
							
						</section>
						<?php endwhile; endif; ?>
						
						<!--<section>
							
							<div class="content">
								<div class="inner">
									<h2>JavaScript</h2>
									<p>JavaScript is the programming language of HTML and the Web.<br>Programming makes computers do what you want them to do.<br>JavaScript is easy to learn.<br>This tutorial will teach you JavaScript from basic to advanced.</p>
									<ul class="actions">
										<li><a href="#" class="button">Learn more</a></li>
									</ul>
								</div>
							</div>
						</section>
						<section>
							
							<div class="content">
								<div class="inner">
									<h2>Bootstrap 3</h2>
									<p>Bootstrap is the most popular HTML, CSS, and JavaScript framework for developing responsive, mobile-first web sites.</p>
									<ul class="actions">
										<li><a href="#" class="button">Learn more</a></li>
									</ul>
								</div>
							</div>
						</section>
					</section>-->

				
					
						

				<!-- Three -->
					<section id="three" class="wrapper style1 fade-up">
						<div class="inner">
							<h2>Get in touch</h2>
							<p>Phasellus convallis elit id ullamcorper pulvinar. Duis aliquam turpis mauris, eu ultricies erat malesuada quis. Aliquam dapibus, lacus eget hendrerit bibendum, urna est aliquam sem, sit amet imperdiet est velit quis lorem.</p>
							<div class="split style1">
								<section>
									<form method="post" action="#">
										<div class="field half first">
											<label for="name">Name</label>
											<input type="text" name="name" id="name" />
										</div>
										<div class="field half">
											<label for="email">Email</label>
											<input type="text" name="email" id="email" />
										</div>
										<div class="field">
											<label for="message">Message</label>
											<textarea name="message" id="message" rows="5"></textarea>
										</div>
										<ul class="actions">
											<li><a href="" class="button submit">Send Message</a></li>
										</ul>
									</form>
								</section>
								
							</div>
						</div>
					</section>

			</div>

		<?php get_footer();?>