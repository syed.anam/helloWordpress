<!-- Footer Section -->
		<footer id="footer">
			<div class="footer-container">
				<div class="container">
					<div class="footer-widget">
					
						<?php
						
							dynamic_sidebar('footer_dynamic_widget');
						?>

						<div class="col-md-4">
							<div class="widget">
								
								<h3 class="widget-title">Contact us</h3>
								<!-- /.widget-title -->
								<div class="address">
									<p>
										<span class="icon"><i class="fa fa-map-marker"></i></span> 
										<span class="txt"> Suite 02, Level 12, Sahera Tropical Center 218 New Elephant Road, Dhaka - 1205 Bangladesh</span>
									</p>
									<p>
										<span class="icon"><i class="fa fa-phone"></i></span> 
										<span class="txt"> +8801923970212, 01776502993</span>
									</p>
									<p>
										<span class="icon"><i class="fa fa-envelope"></i></span> 
										<span class="txt"> info@ bonndu007@gmail.com</span>
									</p>
								</div><!-- /.address -->
							</div><!-- /.widget -->
						</div><!-- /.col-md-4 -->

						<div class="col-md-4">
							<div class="widget quick-link">
								<h3 class="widget-title">Contact us</h3>
								<a href="#">Home</a>
								<a href="#">About</a>
								<a href="#">Blog</a>
								<a href="#">Portfolio</a>
								<a href="#">Content</a>
							</div><!-- /.widget -->
							
						</div><!-- /.col-md-4 -->

						<div class="col-md-4">
							<div class="widget latest-tweets">
								<h3 class="widget-title">Latest Tweets</h3>
								<a href="#">Lipsum dolor sit amet, consectetur adipiscing euismod velit lacinia. Vestibulum tristique...</a>
								<p>Crative Design 6 hours ago , Shuily Yeasin </p>
							</div><!-- /.widget -->

							<div class="widget latest-tweets">
								<a href="#">Lipsum dolor sit amet, consectetur adipiscing euismod velit lacinia. Vestibulum tristique...</a>
								<p>Crative Design 6 hours ago , Shuily Yeasin </p>
							</div><!-- /.widget -->

							<div class="widget latest-tweets">
								<a href="#">Lipsum dolor sit amet, consectetur adipiscing euismod velit lacinia. Vestibulum tristique...</a>
								<p>Crative Design 6 hours ago , Shuily Yeasin </p>
							</div><!-- /.widget -->
						</div><!-- /.col-md-4 -->

					</div><!-- /.footer-widget -->
				</div><!-- /.container -->
			</div><!-- /.footer-container -->
		</footer><!-- /#footer -->
		<!-- Footer Section End -->
		
		<?php wp_footer();?>
	</body>
</html>