<?php

include_once('/inc/custom-post.php');
include_once('/inc/widget.php');

	//add_filter('show_admin_bar','__return_false');
	
	add_theme_support('custom-header');
	add_theme_support('post-thumbnails');
	//add_image_size('id','width','height',boolean);
	add_image_size('team_image',260,270,true);
	
	register_nav_menus(array(
	
		'header_menu'=>'This is Header Menu',
		'footer_menu'=>'This is Footer Menu',	
	
	));
	

function css_js(){

	wp_enqueue_style('awesome',get_template_directory_uri().'/assets/css/font-awesome.min.css',array(),'3.0.0','all');
	wp_enqueue_style('bootstrap',get_template_directory_uri().'/assets/css/bootstrap.min.css',array(),'3.0.0','all');
	wp_enqueue_style('smartmenus',get_template_directory_uri().'/assets/css/jquery.smartmenus.bootstrap.css',array(),'1.0.0','all');
	wp_enqueue_style('style',get_template_directory_uri().'/assets/css/style.css',array(),'1.0.0','all');
	wp_enqueue_style('responsive',get_template_directory_uri().'/assets/css/responsive.css',array(),'1.0.0','all');


	wp_enqueue_script('jquery');
	wp_enqueue_script('plugins',get_template_directory_uri().'/assets/js/plugins.js',array(),'1.0.0',true);
	wp_enqueue_script('functions',get_template_directory_uri().'/assets/js/functions.js',array(),'1.0.0',true);

}
	add_action('wp_enqueue_scripts','css_js');
	
	
	
	function anam_theme(){
		add_theme_support('title-tag');
		
	}
	add_action('after_setup_theme','anam_theme');
?>