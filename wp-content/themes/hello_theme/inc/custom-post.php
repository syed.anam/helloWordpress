<?php

// For custom Service post-type
/* function hello_service(){
	
	$args = array(
		'public'=>true,
		'label'=>'Hello Service',		
		'menu_position'=>5,		
		'supports'=> array('title','editor','thumbnail','author','comments'),		
	);	
	register_post_type('service',$args);	
	
}
add_action('init','hello_service'); */


function hello_service() {
	$labels = array(
		'name'               => _x( 'Hello Services', 'post type general name', 'your-plugin-textdomain' ),
		'singular_name'      => _x( 'Hello Service', 'post type singular name', 'your-plugin-textdomain' ),
		'menu_name'          => _x( 'Hello Services', 'admin menu', 'your-plugin-textdomain' ),
		'name_admin_bar'     => _x( 'Hello Service', 'add new on admin bar', 'your-plugin-textdomain' ),
		'add_new'            => _x( 'Add New', 'Hello Service', 'your-plugin-textdomain' ),
		'add_new_item'       => __( 'Add New Hello Service', 'your-plugin-textdomain' ),
		'new_item'           => __( 'New Hello Service', 'your-plugin-textdomain' ),
		'edit_item'          => __( 'Edit Hello Service', 'your-plugin-textdomain' ),
		'view_item'          => __( 'View Hello Service', 'your-plugin-textdomain' ),
		'all_items'          => __( 'All Hello Services', 'your-plugin-textdomain' ),
		'search_items'       => __( 'Search Hello Services', 'your-plugin-textdomain' ),
		'parent_item_colon'  => __( 'Parent Hello Services:', 'your-plugin-textdomain' ),
		'not_found'          => __( 'No Hello Services found.', 'your-plugin-textdomain' ),
		'not_found_in_trash' => __( 'No Hello Services found in Trash.', 'your-plugin-textdomain' )
	);

	$args = array(
		'labels'             => $labels,
        'description'        => __( 'Description.', 'your-plugin-textdomain' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'Hello Service' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => 5,
		'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
	);

	register_post_type( 'Hello Service', $args );
}
add_action( 'init', 'hello_service' );


// Service Custom meatabox

function service_metabox(){
	
	add_meta_box(
		'our_metabox', //id
		'our_custom_metabox', //title
		'our_meta_callback', //callback
		'Hello Service', //screen
		'normal', //content
		'high' //priority	
	);
	$screen = array('page','post','Hello Service');
	//$screen = array('Hello Service');
	
}
add_action('add_meta_boxes','service_metabox');

function our_meta_callback($post){
	
	wp_nonce_field('save_service_icon_meta','service_nonce');   
	$service_icon = get_post_meta($post->ID,'s_icon',true);
	
	?>
	
	<p>
		<label> Custom Metabox</label>
		<input type="text" name="s_icon" value="<?php echo $service_icon; ?>" />
	</p>
	
	<?php }; 
	
function save_service_icon_meta($post_id){
	
	//Check if our nonce is set
	if(! isset($_POST['service_nonce'])){
		
		return; 
	}
	
	//Verify that the nonce is valid
	if(! wp_verify_nonce($_POST['service_nonce'],'save_service_icon_meta')){
		
		return;
	}
	
	//Make sure that it is valid
	if(! isset($_POST['s_icon'])){
		
		return;
	}
	
	//Sanitize user input
	 $icon = sanitize_text_field($_POST['s_icon']);
	 
	 //Update the meta field in the database
	 update_post_meta($post_id,'s_icon',$icon);
}
add_action('save_post','save_service_icon_meta');


//For Team custom post type
function hello_team() {
	$labels = array(
		'name'               => _x( 'Hello Teams', 'post type general name', 'your-plugin-textdomain' ),
		'singular_name'      => _x( 'Hello Team', 'post type singular name', 'your-plugin-textdomain' ),
		'menu_name'          => _x( 'Hello Teams', 'admin menu', 'your-plugin-textdomain' ),
		'name_admin_bar'     => _x( 'Hello Team', 'add new on admin bar', 'your-plugin-textdomain' ),
		'add_new'            => _x( 'Add New', 'Hello Team', 'your-plugin-textdomain' ),
		'add_new_item'       => __( 'Add New Hello Team', 'your-plugin-textdomain' ),
		'new_item'           => __( 'New Hello Team', 'your-plugin-textdomain' ),
		'edit_item'          => __( 'Edit Hello Team', 'your-plugin-textdomain' ),
		'view_item'          => __( 'View Hello Team', 'your-plugin-textdomain' ),
		'all_items'          => __( 'All Hello Teams', 'your-plugin-textdomain' ),
		'search_items'       => __( 'Search Hello Teams', 'your-plugin-textdomain' ),
		'parent_item_colon'  => __( 'Parent Hello Teams:', 'your-plugin-textdomain' ),
		'not_found'          => __( 'No Hello Teams found.', 'your-plugin-textdomain' ),
		'not_found_in_trash' => __( 'No Hello Teams found in Trash.', 'your-plugin-textdomain' )
	);

	$args = array(
		'labels'             => $labels,
        'description'        => __( 'Description.', 'your-plugin-textdomain' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'Hello Team' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      =>5,
		'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
	);

	register_post_type( 'Hello Team', $args );
}
add_action( 'init', 'hello_team' );



//Pricing custom post type

function pricing_custom_posttype() {
	$labels = array(
		'name'               => _x( 'Hello pricings', 'post type general name', 'your-plugin-textdomain' ),
		'singular_name'      => _x( 'Hello pricing', 'post type singular name', 'your-plugin-textdomain' ),
		'menu_name'          => _x( 'Hello pricings', 'admin menu', 'your-plugin-textdomain' ),
		'name_admin_bar'     => _x( 'Hello pricing', 'add new on admin bar', 'your-plugin-textdomain' ),
		'add_new'            => _x( 'Add New', 'Hello pricing', 'your-plugin-textdomain' ),
		'add_new_item'       => __( 'Add New Hello pricing', 'your-plugin-textdomain' ),
		'new_item'           => __( 'New Hello pricing', 'your-plugin-textdomain' ),
		'edit_item'          => __( 'Edit Hello pricing', 'your-plugin-textdomain' ),
		'view_item'          => __( 'View Hello pricing', 'your-plugin-textdomain' ),
		'all_items'          => __( 'All Hello pricings', 'your-plugin-textdomain' ),
		'search_items'       => __( 'Search Hello pricings', 'your-plugin-textdomain' ),
		'parent_item_colon'  => __( 'Parent Hello pricings:', 'your-plugin-textdomain' ),
		'not_found'          => __( 'No Hello pricings found.', 'your-plugin-textdomain' ),
		'not_found_in_trash' => __( 'No Hello pricings found in Trash.', 'your-plugin-textdomain' )
	);

	$args = array(
		'labels'             => $labels,
                'description'        => __( 'Description.', 'your-plugin-textdomain' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'Hello pricing' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => 5,
		'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
	);

	register_post_type( 'Hello Pricing', $args );
}
add_action( 'init', 'pricing_custom_posttype' );
	
	
// Pricing meta boxes

	function pricing_custom_meta(){
		
		add_meta_box(
		
			'pricing_meta', //ID
			'Pricing Table Meta Box', //title
			'pricing_meta_callback', //callback
			'Hello pricing', //screen
			'normal', //context
			'high' //priority
		);
		
		
	}	
	add_action('add_meta_boxes','pricing_custom_meta');

	function pricing_meta_callback($post){
		wp_nonce_field('save_pricing_meta','pricing_meta_nonce');
		$p_plan = get_post_meta($post->ID,'p_plan',true);
		$p_currency = get_post_meta($post->ID,'p_currency',true);
		$p_ammount = get_post_meta($post->ID,'p_ammount',true);
		$t_duration = get_post_meta($post->ID,'t_duration',true);
		$page = get_post_meta($post->ID,'page',true);
		$domain = get_post_meta($post->ID,'domain',true);
		$hosting = get_post_meta($post->ID,'hosting',true);
		$revision = get_post_meta($post->ID,'revision',true);
		$support = get_post_meta($post->ID,'support',true);
		$curt = get_post_meta($post->ID,'curt',true);
		
		
		?>
		<p>
		<label>Pricing Plan</label>
		<input type="text" name="p_plan" value="<?php echo $p_plan;?>"/>		
		</p>
		<p>
		<label>Pricing Currency</label>
		<input type="text" name="p_currency" value="<?php echo $p_currency;?>"/>		
		</p>
		<p>
		<label>Pricing Ammount</label>
		<input type="text" name="p_ammount" value="<?php echo $p_ammount;?>"/>		
		</p>
		<p>
		<label>Pricing Time Duration</label>
		<input type="text" name="t_duration" value="<?php echo $t_duration;?>"/>		
		</p>
		<p>
		<label>How many page design</label>
		<input type="text" name="page" value="<?php echo $page;?>"/>		
		</p>
		<p>
		<label>Tme duration for Domain </label>
		<input type="text" name="domain" value="<?php echo $domain;?>"/>		
		</p>
		<p>
		<label>Hosting length </label>
		<input type="text" name="hosting" value="<?php echo $hosting;?>"/>		
		</p>
		<p>
		<label>Revision length </label>
		<input type="text" name="revision" value="<?php echo $revision;?>"/>		
		</p>
		<p>
		<label>Support Duration</label>
		<input type="text" name="support" value="<?php echo $support;?>"/>		
		</p>
		<p>
		<label>Order Button</label>
		<input type="text" name="curt" value="<?php echo $curt;?>"/>		
		</p>
		
		
		<?php
	}
	
	function save_pricing_meta($post_id){
		
		// check if our nonce is set		
		if(! isset($_POST['pricing_meta_nonce'])){
			return;
		}
		
		// verify that the nonce is valid
		if(! wp_verify_nonce($_POST['pricing_meta_nonce'],'save_pricing_meta')){
			return;
		}
	
		// wake sure that it is set
		
		if(! isset($_POST['p_plan'])){
			return;
		}
		if(! isset($_POST['p_currency'])){
			return;
		}
		if(! isset($_POST['p_ammount'])){
			return;
		}
		if(! isset($_POST['t_duration'])){
			return;
		}
		if(! isset($_POST['page'])){
			return;
		}
		if(! isset($_POST['domain'])){
			return;
		}
		if(! isset($_POST['hosting'])){
			return;
		}
		if(! isset($_POST['revision'])){
			return;
		}
		if(! isset($_POST['support'])){
			return;
		}
		if(! isset($_POST['curt'])){
			return;
		}
		
		
		// Sanitize user input
		$my_p_plan = sanitize_text_field($_POST['p_plan']);
		$my_p_currency = sanitize_text_field($_POST['p_currency']);
		$my_p_ammount = sanitize_text_field($_POST['p_ammount']);
		$my_t_duration = sanitize_text_field($_POST['t_duration']);
		$my_page = sanitize_text_field($_POST['page']);
		$my_domain = sanitize_text_field($_POST['domain']);
		$my_hosting = sanitize_text_field($_POST['hosting']);
		$my_revision = sanitize_text_field($_POST['revision']);
		$my_support = sanitize_text_field($_POST['support']);
		$my_curt = sanitize_text_field($_POST['curt']);
		
		
		// update the meta field in the database
		update_post_meta($post_id,'p_plan',$my_p_plan );
		update_post_meta($post_id,'p_currency',$my_p_currency);
		update_post_meta($post_id,'p_ammount',$my_p_ammount);
		update_post_meta($post_id,'t_duration',$my_t_duration);
		update_post_meta($post_id,'page',$my_page);
		update_post_meta($post_id,'domain',$my_domain);
		update_post_meta($post_id,'hosting',$my_hosting);
		update_post_meta($post_id,'revision',$my_revision);
		update_post_meta($post_id,'support',$my_support);
		update_post_meta($post_id,'curt',$my_curt);
		
		
	}
	add_action('save_post','save_pricing_meta');
	
	
	
// Portfolio Custom post-type



function portfolio_post_type() {
	$labels = array(
		'name'               => _x( 'Portfolios', 'post type general name', 'your-plugin-textdomain' ),
		'singular_name'      => _x( 'Portfolio', 'post type singular name', 'your-plugin-textdomain' ),
		'menu_name'          => _x( 'Portfolios', 'admin menu', 'your-plugin-textdomain' ),
		'name_admin_bar'     => _x( 'Portfolio', 'add new on admin bar', 'your-plugin-textdomain' ),
		'add_new'            => _x( 'Add New', 'Portfolio', 'your-plugin-textdomain' ),
		'add_new_item'       => __( 'Add New Portfolio', 'your-plugin-textdomain' ),
		'new_item'           => __( 'New Portfolio', 'your-plugin-textdomain' ),
		'edit_item'          => __( 'Edit Portfolio', 'your-plugin-textdomain' ),
		'view_item'          => __( 'View Portfolio', 'your-plugin-textdomain' ),
		'all_items'          => __( 'All Portfolios', 'your-plugin-textdomain' ),
		'search_items'       => __( 'Search Portfolios', 'your-plugin-textdomain' ),
		'parent_item_colon'  => __( 'Parent Portfolios:', 'your-plugin-textdomain' ),
		'not_found'          => __( 'No Portfolios found.', 'your-plugin-textdomain' ),
		'not_found_in_trash' => __( 'No Portfolios found in Trash.', 'your-plugin-textdomain' )
	);

	$args = array(
		'labels'             => $labels,
                'description'        => __( 'Description.', 'your-plugin-textdomain' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'Portfolio' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => 5,
		'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
	);

	register_post_type( 'portfolio', $args );
}	

add_action( 'init', 'portfolio_post_type' );



// Portfolio taxonomy register

function portfolio_taxonomy(){
	
	// Add new taxonomy, make it hierarchical (like categories)
	$labels = array(
		'name'              => _x( 'Helllo_taxonomys', 'taxonomy general name' ),
		'singular_name'     => _x( 'Helllo_taxonomy', 'taxonomy singular name' ),
		'search_items'      => __( 'Search Helllo_taxonomys' ),
		'all_items'         => __( 'All Helllo_taxonomys' ),
		'parent_item'       => __( 'Parent Helllo_taxonomy' ),
		'parent_item_colon' => __( 'Parent Helllo_taxonomy:' ),
		'edit_item'         => __( 'Edit Helllo_taxonomy' ),
		'update_item'       => __( 'Update Helllo_taxonomy' ),
		'add_new_item'      => __( 'Add New Helllo_taxonomy' ),
		'new_item_name'     => __( 'New Helllo_taxonomy Name' ),
		'menu_name'         => __( 'Helllo_taxonomy' ),
	);
	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'Helllo_taxonomy' ),
	);
	
	register_taxonomy('hello_taxonomy',array('portfolio'),$args);
}
add_action('init','portfolio_taxonomy');


	
