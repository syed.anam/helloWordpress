<?php

function footer_widget() {
    register_sidebar( array(
        'name' =>'Footer Widget',
        'id' => 'footer_dynamic_widget',
        'description' => 'this is our first sidebar',
        'before_widget' => '<div class="col-md-4"><div class="widget">',
		'after_widget'  => '</div></div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
    ) );
}

add_action( 'widgets_init', 'footer_widget' );
?>