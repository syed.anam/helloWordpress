<?php get_header();?>
		
		<!-- Slider Section -->
		<section id="heading">
			<div class="overlay">
				<div class="container">
					<h1 class="title">Blog</h1>
				</div><!-- /.container -->
			</div><!-- /.overlay -->
		</section><!-- /#slider -->
		<!-- Slider Section End -->


		<div class="blog-content">
			<div class="container">
				<div class="row">
					<div class="col-md-8">
						<div class="post-container">
						
						<?php
						
							if(have_posts()) : while(have_posts()) : the_post();
						?>
							
							<div class="single-post">
								<p class="entry-date"><?php the_time('F j,Y')?></p>
								<a href="#" class="entry-title"><?php the_title()?></a>
								<span class="auther">Posted By: <a href="<?php the_author_link();?>"><?php the_author()?></a></span>
								<div class="entry-comtent"> 
									
									<?php the_content();?>
									
								</div>
							</div><!-- /.single-post -->
							
							<?php  endwhile; endif; ?>
							
						</div>
						<hr>
					</div><!-- /.col-md-8 -->

					<div class="col-md-4">
						<div class="sidebar">

							<!-- Search Bar -->
							<div class="widget blog-search-bar">
								<h4 class="title">Search</h4>
								<form class="form-search" method="get" id="s" action="/">
									<div class="input-append">
										<input class="form-control input-medium search-query" type="text" name="s" placeholder="Search" required>
										<button class="add-on" type="submit"><i class="fa fa-search"></i></button>
									</div><!-- /.input-append -->
								</form><!-- /.form-search -->
							</div><!-- /.blog-search-bar -->
							<!-- Search Bar End -->

							<!-- Recent Article -->
							<div class="widget article">
								<h4 class="title">Recent Articles</h4>
								<ul class="article-list">
									<li><a href="#">Article-1</a></li>
									<li><a href="#">Article-2</a></li>
									<li><a href="#">Article-3</a></li>
									<li><a href="#">Article-4</a></li>
									<li><a href="#">Article-5</a></li>
								</ul>
							</div><!-- /.widget article -->
							<!-- Recent Article End -->

							<!-- About Me -->
							<div class="widget about-me">
								<h4 class="title">About Me</h4>
								<img src="images/Rectangle.jpg" alt="My Pictures">
								<p>and backward-comSeamlessly disseminate team building e-marketspatible vortals. Comenabled meta-services rather than </p>
							</div><!-- /.widget about-me -->
							<!-- About Me End -->

							<!-- Categories -->
							<div class="widget categories">
								<h4 class="title">Categories</h4>
								<select>
								  <option value="dhaka">Dhaka</option>
								  <option value="ctg">Chittagong</option>
								  <option value="raj">Rajshahi</option>
								  <option value="khulna">Khulna</option>
								</select>
							</div><!-- /.widget categories -->
							<!-- Categories End -->

						</div><!-- /.sidebar -->
					</div><!-- /.col-md-4 -->
				</div>
			</div><!-- /.container -->
		</div><!-- /.blog-content -->

		
		<?php get_footer();?>