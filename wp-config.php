<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'hello-wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '^H;o~x{4TG.=-i{f5$$Sw#:kJZTfp~l^/-h{,:Scg;a)OT^w>*9coB#ovPTcHRr~');
define('SECURE_AUTH_KEY',  'q2)*RTw~v{<FHCC&4Z=XWh7Dl4nKw0OKN)>{9Fj|SY8:]7,!TQgC&#W%a}*YP$S ');
define('LOGGED_IN_KEY',    'vKOoz!)*a20-.,T*6}mY1 PuW.uwm=HIN upKH 1vL^jNE.>,2/9;rpWCpQLqcc<');
define('NONCE_KEY',        '|G!Kn7qz0&t/RYDBsm3S]]$oz7 `+hv8g!2r0Mdpy<HlOeB/GbUCugP^Jg6Qa*SZ');
define('AUTH_SALT',        'JF*^L n0*9)x(}JjY)`mh*x[gks2s%dF|Ee-$?a^#N~YIwmgkr_ (ydpD:eELn@f');
define('SECURE_AUTH_SALT', '&3Y<7}4b9`FH`!~CB%+YvjDZ+.Ig~4sBTVq!eQ@mt`y@jiV_o.^% F#$rHmS_+2y');
define('LOGGED_IN_SALT',   'F)BB8X9=,)>WrD@QyJv)a#sBh,e)i5Wni,]iav.nSs[r`RD?gqd@sT)AU];izUJ5');
define('NONCE_SALT',       'K%&*-SQM$cL:xrhJs]8G +~?z8dzu0IWs`Wc{Cp`2wDH?Ii)oL!4Q@pC]EpX{c}7');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
